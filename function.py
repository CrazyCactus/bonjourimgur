import random
import requests
from imgurpython import ImgurClient
from math import *

#Information du client
client_id = 'fae94e7e9fa14da'
client_secret = '63461bcec20412e30e152471d8686a16965e76b8'
client = ImgurClient(client_id, client_secret)

def crosstag(response, tag):
    cross = []
    for item in response.items:
        for t in item.tags:
            if t['name'] == tag[1]:
                cross.append(item.link)
            # print(tagu['name'])
    if cross:
        rdpic = random.randint(1, len(cross))
        link = cross[rdpic-1]
    else:
        print('tour de request croisée')
        response = request(tag)
        link = crosstag(response, tag)
        # print('---------------------------')
    return link

def rdpagegen(tagword):
    total_items = client.gallery_tag(tagword).total_items
    pagemax = ceil(total_items/50)
    return random.randint(1, pagemax)


def request(tag,rd=True):
    if rd:
        if isinstance(tag, list):
            rdpage = rdpagegen(tag[0])
            response = client.gallery_tag(tag[0], sort='newest', page=rdpage)
        else:
            rdpage = rdpagegen(tag)
            response = client.gallery_tag(tag, sort='newest', page=rdpage)

        if response.items == None:
            print('tour de request')
            response = request(tag)
    else:
        response = client.gallery_tag(tag, sort='newest')
    return response

def requestapi(tag,rd=True):
    if rd:
        headers = {'Authorization': 'Client-ID e125ba16f72b82c'}
        if isinstance(tag, list):
            rdpage = rdpagegen(tag[0])
            response = requests.request("GET",'https://api.imgur.com/3/gallery/t/{}/newest/0/{}'.format(tag,rdpage),headers=headers)
            print(response)
        else:
            rdpage = rdpagegen(tag)
            response = requests.request("GET",'https://api.imgur.com/3/gallery/t/{}/newest/0/{}'.format(tag,rdpage),headers=headers)
            print(response)

        if response.items == None:
            print('tour de request')
            response = request(tag)
    else:
        response = client.gallery_tag(tag, sort='newest')
    return response


def askpic(tag,index):
    if index == None:
        response = request(tag)
        rdpic = random.randint(1, len(response.items))

    elif isinstance(index, int):
        response = request(tag,False)
        rdpic = index

    if isinstance(tag, list):
        link = crosstag(response, tag)
    else:
        link = response.items[rdpic-1].link

    print(link)
